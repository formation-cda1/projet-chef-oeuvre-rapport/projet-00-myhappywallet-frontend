import {ReactComponent as Dashboard} from "./dashboard.svg"
import {ReactComponent as logo} from "./logo.svg";
import {ReactComponent as Home} from "./dashboard.svg";
import {ReactComponent as Calendar} from "./calendar.svg";
import {ReactComponent as Projects} from "./target.svg";
import {ReactComponent as List} from "./list.svg";
import {ReactComponent as PowerOff} from "./power-off.svg"
export default {
    Dashboard, logo, Home, Calendar, Projects, List,PowerOff
}