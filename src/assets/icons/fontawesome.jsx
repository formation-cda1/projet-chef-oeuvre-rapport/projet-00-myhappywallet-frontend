import { library } from '@fortawesome/fontawesome-svg-core';
// import your icons
import { faCode, faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';

library.add(
  faCode,
  faChevronCircleLeft
  // more icons go here
);